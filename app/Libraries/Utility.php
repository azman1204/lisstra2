<?php
namespace App\Libraries;

class Utility {
    // return array of jabatan ['01' => 'KDN', ...]
    // Utility::getJabatan()
    static function getJabatan() {
        $arr_jab = (new \App\Models\Jabatan())->findAll();
        $arr = [];
        foreach($arr_jab as $jab) {
            $kod_jabatan = $jab['kod_jabatan'];
            $arr[$kod_jabatan] = $jab['keterangan_jabatan'];
        }
        return $arr;
    }
}