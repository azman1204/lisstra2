<?php
namespace App\Controllers;
use App\Models\JTI;

class HelloController extends BaseController {
    function index() {
        $jti = new JTI();
        $data = $jti->findAll();
        var_dump($data);
        return 'Hello World';
    }

    function genPdf() {
        $dompdf = new \Dompdf\Dompdf();
        $dompdf->loadHtml(view('hello'));
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream();
    }
}