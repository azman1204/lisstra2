<?php
namespace App\Controllers;
use App\Models\Jabatan;
use App\Models\JTI;
use App\Models\JTISA;
use App\Models\POL;
use App\Models\StatusPOL;
use App\Models\StatusTU;
use App\Models\TU;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;

class Report2Controller extends BaseController {
    
    function index() {
        $data = ['tahun' => '', 'jabatan' => 'all'];
        if ($this->request->is('post')) {
            // buat carian
            $data = $this->request->getPost();
            $tahun = $data['tahun'];
            $jabatan = $data['jabatan'];
            $where = '';
            if (! empty($data['tahun'])) {
                $where = " AND tahun = $tahun ";
            }

            if ($data['jabatan'] !== 'all') {
                $where .= " AND jabatan = '$jabatan' ";
            }
        } else {
            $where = '';
            // klik dari menu
        }

        $db = db_connect();
        $sql = "SELECT status_stat, jabatan, COUNT(*) AS bil
                FROM tr_statistik
                WHERE status_stat = 'B' $where
                GROUP BY status_stat, jabatan";
        $query = $db->query($sql);
        $rs1 = $query->getResult(); // return aray of obj
        //d($rs1);

        $jabatan = new Jabatan();
        $arr_jab = $jabatan->findAll();
        // generate 10 rows x 19 columns
        $arr1 = []; // row
        for($i=1; $i<= 10; $i++) {
            for($j=1; $j<=20; $j++) {
                $arr1[$i - 1][] = '-';
            }
        }

        // isi cell bil dan jabatan (0,1)
        $no = 0;
        foreach($arr_jab as $jab) {
            $arr1[$no][0] = $no + 1; // colum 0
            $arr1[$no][1] = $jab['kod_jabatan'];// column 1
            $arr1[$no][19] = $jab['keterangan_jabatan'];// column 19
            $no++;
        }

        // isi cell status "BARU" (3)
        $no = 0;
        foreach($arr1 as $arr) {
            $kod_jab = $arr[1];
            //echo $kod_jab . '<br>';
            foreach ($rs1 as $rs) {
                if ($kod_jab == $rs->jabatan) {
                    $arr1[$no][3] = $rs->bil;
                }
            }
            $no++;
        }

        // isi cell (4-8)
        $sql = "SELECT stat_JTI, jabatan, COUNT(*) AS bil
                FROM tr_statistik
                WHERE stat_JTI != '' $where
                GROUP BY stat_JTI, jabatan";
        $query = $db->query($sql);
        $rs1 = $query->getResult(); // return aray of obj
        $no = 0;
        foreach($arr1 as $arr) {
            $kod_jab = $arr[1];
            foreach ($rs1 as $rs) {
                if ($kod_jab == $rs->jabatan) {
                    if ($rs->stat_JTI == '01') {
                        $arr1[$no][4] = $rs->bil;
                    } else if ($rs->stat_JTI == '02') {
                        $arr1[$no][5] = $rs->bil;
                    } else if ($rs->stat_JTI == '03') {
                        $arr1[$no][6] = $rs->bil;
                    } else if ($rs->stat_JTI == '04') {
                        $arr1[$no][7] = $rs->bil;
                    } else if ($rs->stat_JTI == '05') {
                        $arr1[$no][8] = $rs->bil;
                    }
                }
            }
            $no++;
        }

        // isi cell (9-13)
        $sql = "SELECT stat_JPICT, jabatan, COUNT(*) AS bil
                FROM tr_statistik
                WHERE stat_JPICT != '' $where
                GROUP BY stat_JPICT, jabatan";
        $query = $db->query($sql);
        $rs1 = $query->getResult(); // return aray of obj
        $no = 0;
        foreach($arr1 as $arr) {
            $kod_jab = $arr[1];
            foreach ($rs1 as $rs) {
                if ($kod_jab == $rs->jabatan) {
                    if ($rs->stat_JPICT == '01') {
                        $arr1[$no][9] = $rs->bil;
                    } else if ($rs->stat_JPICT == '02') {
                        $arr1[$no][10] = $rs->bil;
                    } else if ($rs->stat_JPICT == '03') {
                        $arr1[$no][11] = $rs->bil;
                    } else if ($rs->stat_JPICT == '04') {
                        $arr1[$no][12] = $rs->bil;
                    } else if ($rs->stat_JPICT == '05') {
                        $arr1[$no][13] = $rs->bil;
                    }
                }
            }
            $no++;
        }

        // isi cell (18-18)
        $sql = "SELECT stat_JTISA, jabatan, COUNT(*) AS bil
                FROM tr_statistik
                WHERE stat_JTISA != '' $where
                GROUP BY stat_JTISA, jabatan";
        $query = $db->query($sql);
        $rs1 = $query->getResult(); // return aray of obj
        $no = 0;
        foreach($arr1 as $arr) {
            $kod_jab = $arr[1];
            foreach ($rs1 as $rs) {
                if ($kod_jab == $rs->jabatan) {
                    if ($rs->stat_JTISA == '01') {
                        $arr1[$no][14] = $rs->bil;
                    } else if ($rs->stat_JTISA == '02') {
                        $arr1[$no][15] = $rs->bil;
                    } else if ($rs->stat_JTISA == '03') {
                        $arr1[$no][16] = $rs->bil;
                    } else if ($rs->stat_JTISA == '04') {
                        $arr1[$no][17] = $rs->bil;
                    } else if ($rs->stat_JTISA == '05') {
                        $arr1[$no][18] = $rs->bil;
                    }
                }
            }
            $no++;
        }

        // isi cell (2)
        $sql = "SELECT COUNT(*) AS bil, jabatan
                FROM tr_statistik
                WHERE 1=1 $where
                GROUP BY jabatan";
        $query = $db->query($sql);
        $rs1 = $query->getResult(); // return aray of obj
        $no = 0;
        foreach($arr1 as $arr) {
            $kod_jab = $arr[1];
            foreach ($rs1 as $rs) {
                if ($kod_jab == $rs->jabatan) {
                    $arr1[$no][2] = $rs->bil;
                }
            }
            $no++;
        }
        $arr_jab2 = \App\Libraries\Utility::getJabatan();
        $arr_jab2['all'] = 'Semua Jabatan';
        
        // kira jumlah bagi setiap column (last row)
        $arr_jum = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
        foreach($arr1 as $row) {
            $col_jum = 0;
            for($col=2; $col<=18; $col++) {
                if ($row[$col] !== '-') {
                    $arr_jum[$col_jum] += $row[$col];
                }
                $col_jum++;
            }
        }
        return view('report_statistik', 
        compact('arr1', 'arr_jab', 'data', 'arr_jab2', 'arr_jum'));
    }

    function laporanJTI() {
        // handle carian
        $where = '';
        $data = [
            'tahun'   => '',
            'bulan'   => '',
            'jabatan' => ''
        ];

        if ($this->request->is('post') || $this->request->getGet('print') || $this->request->getGet('excel')) {
            // click button cari
            $data = $this->request->getGetPost();
            $tahun = $data['tahun'];
            $bulan = $data['bulan'];
            $jabatan = $data['jabatan'];
            
            if (! empty($tahun)) {
                $where .= "AND tarikh LIKE '$tahun-%'";
            }

            if (! empty($bulan)) {
                $where .= "AND tarikh LIKE '%-$bulan-%'";
            }

            if ($jabatan !== 'all') {
                $where .= "AND kod_jabatan = '$jabatan'";
            }
        }

        $db = db_connect();
        $sql = "SELECT * FROM tr_jti WHERE 1=1 $where ORDER BY bil";
        $query = $db->query($sql);
        //$jti = new JTI();
        //$rows = $jti->orderBy('bil')->where()->findAll();
        $rows = $query->getResultArray();
        
        $sql = "SELECT COUNT(*) AS count, bil FROM tr_jti WHERE 1=1 $where GROUP BY bil ORDER BY bil";
        $query = $db->query($sql);
        $rs = $query->getResult();
        $jabatan = new Jabatan();
        $arr_jab = $jabatan->findAll();
        $arr_jab2 = [];
        foreach ($arr_jab as $jab) {
            $arr_jab2[$jab['kod_jabatan']] = $jab['keterangan_jabatan'];
        }
        $bulan = [
            '01' => 'Jan',
            '02' => 'Feb',
            '03' => 'Mar',
            '04' => 'Apr',
            '05' => 'Mei',
            '06' => 'Jun',
            '07' => 'Jul',
            '08' => 'Aug',
            '09' => 'Sep',
            '10' => 'Oct',
            '11' => 'Nov',
            '12' => 'Dec'
        ];

        if ($this->request->getGet('print')) {
            $dompdf = new \Dompdf\Dompdf();
            $print = true;
            $html = view('laporan_jti_detail', 
            compact('rows', 'rs', 'arr_jab2', 'bulan', 'data', 'print'));
            $dompdf->loadHtml($html);
            $dompdf->setPaper("A4", "landscape");
            $dompdf->render();
            $dompdf->stream("laporan_jti.pdf");
        } else if ($this->request->getGet('excel')) {
            $ss = new Spreadsheet();
            $ws = $ss->getActiveSheet();
            $ws->setCellValue('A1', 'Bil');
            $ws->getStyle('A1')
                ->getBorders()
                ->getOutline()
                ->setBorderStyle(Border::BORDER_THICK);
            $ws->setCellValue('B1', 'Tarikh');
            $ws->setCellValue('C1', 'Pengerusi');
            $ws->setCellValue('D1', 'No');
            $ws->setCellValue('E1', 'Jabatan');
            $ws->setCellValue('F1', 'Nama Ringkas Projek');
            $ws->setCellValue('G1', 'Kos (RM)');
            $ws->setCellValue('H1', 'Tempoh');
            $ws->setCellValue('I1', 'Sumber Peruntukan');
            $ws->setCellValue('J1', 'Ulasan JTI');
            $ws->setCellValue('K1', 'Tempoh Terima Permohonan');
            $i = 2;
            foreach ($rows as $row) {
                $ws->setCellValue("A$i", $row['bil']);
                $ws->setCellValue("B$i", $row['tarikh']);
                $ws->setCellValue("C$i", $row['pengerusi']);
                $ws->setCellValue("D$i", $row['no']);
                $ws->setCellValue("E$i", $arr_jab2[$row['kod_jabatan']]);
                $ws->setCellValue("F$i", $row['tajuk']);
                $ws->setCellValue("G$i", $row['kos']);
                $ws->setCellValue("H$i", $row['tempoh']);
                $ws->setCellValue("I$i", $row['sumber_peruntukan']);
                $ws->setCellValue("J$i", $row['ulasan_jti']);
                $ws->setCellValue("K$i", $row['tarikh_terima_permohonan']);
                $i++;
            }

            $writer = new Xlsx($ss);
            $writer->save('hello.xlsx');
            return redirect()->to(base_url('hello.xlsx'));
        } else {
            return view('laporan_jti', compact('rows', 'rs', 'arr_jab2', 'bulan', 'data'));
        }
    }

    function laporanJTISA() {
        $jtisa = new JTISA();
        $where = '';
        $data = ['tahun' => '', 'bulan' => '', 'jabatan' => ''];
        if ($this->request->is('post')) {
            // submit form carian
            $data = $this->request->getPost();
            $tahun = $data['tahun'];
            $bulan = $data['bulan'];
            $jabatan = $data['jabatan'];
            
            if (!empty($tahun)) {
                // Y-m-d
                $where .= " AND tarikh like '$tahun-%'";
            }

            if (!empty($bulan)) {
                // Y-m-d
                $where .= " AND tarikh like '%-$bulan-%'";
            }

            if ($jabatan !== 'all') {
                // Y-m-d
                $where .= " AND kod_jabatan = '$jabatan'";
            }
        }

        $query = $jtisa->where("1=1 $where");
        $rows = $query->findAll();
        $arr_jab = (new Jabatan())->findAll();
        $bulan = [
            '01' => 'Jan',
            '02' => 'Feb',
            '03' => 'Mar',
            '04' => 'Apr',
            '05' => 'Mei',
            '06' => 'Jun',
            '07' => 'Jul',
            '08' => 'Aug',
            '09' => 'Sep',
            '10' => 'Oct',
            '11' => 'Nov',
            '12' => 'Dec'
        ];
        return view('laporan_jtisa', 
        compact('rows', 'arr_jab', 'bulan', 'data'));
    }

    function laporanKursus() {
        $where = '';
        $data = ['tahun' => '', 'bulan' => '', 'pol' => '', 'status' => ''];
        if ($this->request->is('post') || $this->request->getGet('print')) {
            // submit search form
            $data = $this->request->getGetPost();
            $tahun = $data['tahun'];
            $bulan = $data['bulan'];
            $pol = $data['pol'];
            $status = $data['status'];

            if (! empty($tahun)) {
                $where .= " AND tarikh_cadangan LIKE '$tahun-%'";
            }

            if (! empty($bulan)) {
                $where .= " AND tarikh_cadangan LIKE '%-$bulan-%'";
            }

            if (! empty($pol)) {
                $where .= " AND kod_POL = '$pol'";
            }

            if (! empty($status)) {
                $where .= " AND status_POL = '$status'";
            }
        } else {
            // klik dari menu
        }

        $pol_internal = (new POL())
                        ->where("kod_pol = '01' $where")
                        ->findAll();
        $pol_swasta = (new POL())
                        ->where("kod_pol = '02' $where")
                        ->findAll();
        $cat = ['INHOUSE', 'SWASTA DALAM NEGERA'];

        // senarai status kursus
        $status = (new StatusPOL())->findAll();
        $arr = [
            ['Peratus Pelaksanaan Kursus', 0, 0],
            ['Bilangan Kursus', 0, 0]
        ];

        $db = db_connect();
        $bil_internal = 0;
        $bil_external = 0;
        foreach ($status as $sts) {
            $arr2 = [$sts['keterangan_statusPOL'], 0, 0];
            $sql = "SELECT kod_POL, COUNT(*) AS bil 
                    FROM tr_pol 
                    WHERE status_POL = '{$sts['kod_statusPOL']}' $where
                    GROUP BY kod_POL";
            $query = $db->query($sql);
            $rs = $query->getResult(); // return array of obj
            foreach($rs as $r) {
                if ($r->kod_POL == '01') {
                    // internal
                    $arr2[1] = $r->bil;
                    $bil_internal += $r->bil; // $bil_internal = $bil_internal + $r->bil
                } else if ($r->kod_POL == '02') {
                    // external
                    $arr2[2] = $r->bil;
                    $bil_external += $r->bil;
                }
            }
            $arr[] = $arr2;
        }
        $arr[1][1] = $bil_internal;
        $arr[1][2] = $bil_external;

        // kira peratus pelaksanaan
        foreach($arr as $a) {
            if ($a[0] == 'SELESAI') {
                $arr[0][1] = $a[1] == 0 ? 0 : $a[1] / $bil_internal * 100;
                $arr[0][2] = $a[2] == 0 ? 0 : $a[2] / $bil_external * 100;
            }
        }

        // kira total peruntukandan belanja
        $sql = "SELECT SUM(peruntukan_diluluskan) AS peruntukan, SUM(kos_digunakan) AS belanja
                FROM tr_pol
                WHERE 1=1 $where";
        $query = $db->query($sql);
        $total = $query->getRow();// return object
        //d($total);

        $bulan = [
            '01' => 'Jan',
            '02' => 'Feb',
            '03' => 'Mar',
            '04' => 'Apr',
            '05' => 'Mei',
            '06' => 'Jun',
            '07' => 'Jul',
            '08' => 'Aug',
            '09' => 'Sep',
            '10' => 'Oct',
            '11' => 'Nov',
            '12' => 'Dec'
        ];

        if(! empty($this->request->getGet('print'))) {
            // pdf
            $print = true;
            $dompdf = new \Dompdf\Dompdf();
            $html = view('laporan_kursus_detail', 
            compact('pol_internal', 'pol_swasta', 'cat', 'arr', 'total', 'bulan', 'status', 'print'));
            $dompdf->loadHtml($html);
            $dompdf->setPaper("A3", "landscape");
            $dompdf->render();
            $dompdf->stream();
        } else {
            return view('laporan_kursus', 
            compact('pol_internal', 'pol_swasta', 'cat', 'arr', 'total', 'bulan', 'status', 'data'));
        }
    }

    // laporan teknologi update (TU)
    function laporanTu() {
        $where = '';
        $data = ['tahun' => '', 'bulan' => '', 'status' => ''];
        if($this->request->is('post') || $this->request->getGet('print') || $this->request->getGet('excel')) {
            // filter data / submit form
            $data = $this->request->getGetPost();
            $tahun = $data['tahun'];
            $bulan = $data['bulan'];
            $status = $data['status'];
            if (! empty($tahun)) {
                $where .= "AND tarikh_terima LIKE '$tahun-%'";
            }

            if (! empty($bulan)) {
                $where .= "AND tarikh_terima LIKE '%-$bulan-%'";
            }

            if (! empty($status)) {
                $where .= "AND status_tu = '$status'";
            }
        }

        $db = db_connect();
        $sql = "SELECT status_tu, count(*) AS bil
                FROM tr_tu 
                WHERE 1=1 $where
                GROUP BY status_tu";
        $query = $db->query($sql);
        $rs = $query->getResult();

        $stat = [];
        $status = (new StatusTU)->findAll();
        $jumlah = 0;
        foreach($status as $sts) {
            $arr = [$sts['keterangan_tu'], 0];
            foreach ($rs as $row) {
                if ($sts['kod_tu'] == $row->status_tu) {
                    $arr[1] = $row->bil;
                    $jumlah += $row->bil;
                }
            }
            $stat[] = $arr;
        }
        $tu = (new TU())->where("1=1 $where")->findAll();
        $stat2 = $this->stat($where);
        $bulan = [
            '01' => 'Jan',
            '02' => 'Feb',
            '03' => 'Mar',
            '04' => 'Apr',
            '05' => 'Mei',
            '06' => 'Jun',
            '07' => 'Jul',
            '08' => 'Aug',
            '09' => 'Sep',
            '10' => 'Oct',
            '11' => 'Nov',
            '12' => 'Dec'
        ];

        //d($this->request->getGet('print'));exit;

        if(! empty($this->request->getGet('excel'))) {
            $print = true;
            $html = view('laporan_tu_detail', 
            compact('tu', 'stat', 'jumlah', 'stat2', 'status', 'bulan', 'print'));
            header('Content-disposition: attachment; filename=laporan_tu.xls');
            header('Content-type: application/vnd.ms-excel');
            return $html;
        } else if(! empty($this->request->getGet('print'))) {
            // pdf
            $print = true;
            $dompdf = new \Dompdf\Dompdf();
            $html = view('laporan_tu_detail', 
            compact('tu', 'stat', 'jumlah', 'stat2', 'status', 'bulan', 'print'));
            $dompdf->loadHtml($html);
            $dompdf->setPaper("A4", "landscape");
            $dompdf->render();
            $dompdf->stream();
        } else {
            // html biasa
            return view('laporan_tu', 
            compact('tu', 'stat', 'jumlah', 'stat2', 'status', 'bulan', 'data'));
        }
        
    }

    // return statistik bil projek mengikut status dan pengerusi
    public function stat($where = '') {
        $arr =[];
        $status = (new StatusTU)->findAll();
        foreach ($status as $sts) {
            $arr2 = [$sts['kod_tu'], $sts['keterangan_tu'], 0, 0, 0, 0];
            $arr[] = $arr2;
        }
        //d($arr);
        $sql = "SELECT status_tu, pengerusi_tu, COUNT(*) AS bil 
                FROM tr_tu 
                WHERE 1=1 $where
                GROUP BY status_tu, pengerusi_tu";
        $db = db_connect();
        $query = $db->query($sql);
        $rs = $query->getResult();

        foreach ($rs as $row) {
            $i = 0;
            foreach ($arr as $stat) {
                if ($row->status_tu == $stat[0]) {
                    if ($row->pengerusi_tu == 'SUB (IT)') {
                        $arr[$i][5] = $row->bil;
                    } else if ($row->pengerusi_tu == 'KSU') {
                        $arr[$i][3] = $row->bil;
                    } else if ($row->pengerusi_tu == 'MDN') {
                        $arr[$i][2] = $row->bil;
                    } else if ($row->pengerusi_tu == 'TKSU(P)') {
                        $arr[$i][4] = $row->bil;
                    }
                }
                $i++;
            }
        }
        return $arr;
    }
}