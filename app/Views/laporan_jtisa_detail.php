
<h4 class="text-center">
LAPORAN KELULUSAN JAWATANKUASA ICT SEKTOR AWAM (JTISA) <br>
KEMENTERIAN DALAM NEGERI 
</h4>

<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Bil</th>
            <th>Jabatan</th>
            <th>Tarikh</th>
            <th>Bil.</th>
            <th>Bil. Kertas</th>
            <th>Projek</th>
            <th>Kos (RM)</th>
            <th>Kelulusan</th>
            <th>Catatan</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        foreach($rows as $jtisa) : ?>
        <tr>
            <td><?= $no++ ?>.</td>
            <td><?= $jtisa['kod_jabatan'] ?></td>
            <td><?= $jtisa['tarikh'] ?></td>
            <td><?= $jtisa['bil_jtisa'] ?></td>
            <td><?= $jtisa['bil_kertas'] ?></td>
            <td><?= $jtisa['tajuk'] ?></td>
            <td><?= $jtisa['kos'] ?></td>
            <td><?= $jtisa['kelulusan'] ?></td>
            <td><?= $jtisa['catatan'] ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>