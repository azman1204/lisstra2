<h4>STATUS PERLAKSANAAN PELAN OPERASI LATIHAN (POL)</h4>

<?php
foreach($cat as $c) : 
    if ($c == 'INHOUSE')
        $rows = $pol_internal;
    else
        $rows = $pol_swasta;?>
    
    <div class="row flex-nowrap overflow-auto">
        <table class="table table-bordered table-striped">
            <thead >
                <tr class="table-primary">
                    <th rowspan="2">Bil</th>
                    <th rowspan="2">Kod Kursus</th>
                    <th rowspan="2">Modul</th>
                    <th rowspan="2">Siri</th>
                    <th rowspan="2">Kumpulan Sasaran</th>
                    <th rowspan="2">Pemilik</th>
                    <th rowspan="2">Anggaran Bil. Peserta</th>
                    <th colspan="2">Jumlah Penyertaan</th>
                    <th rowspan="2">Tempat</th>
                    <th rowspan="2">Tarikh Cadangan</th>
                    <th rowspan="2">Tempoh (Hari)</th>
                    <th rowspan="2">Status</th>
                    <th rowspan="2">Peruntukan Diluluskan (RM)</th>
                    <th rowspan="2">Kos Digunakan (RM)</th>
                    <th rowspan="2">Anggaran Bayaran Penceramah</th>
                    <th rowspan="2">Baki Perbelanjaan (RM)</th>
                    <th rowspan="2">Catatan</th>
                </tr>
                <tr class="table-primary">
                    <th>Lelaki</th>
                    <th>Perempuan</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="18" class="table-info">
                        <?= $c ?>
                    </td>
                </tr>

                <?php
                $bil = 1;
                $jumlah = 0;
                foreach($rows as $p) : 
                    $jumlah += $p['peruntukan_diluluskan'];
                    $p2 = new \App\Models\POL(); // cari by pk
                ?>
                <tr>
                    <td><?= $bil++ ?>.</td>
                    <td><?= $p['kod_kursus'] ?></td>
                    <td><?= $p['nama_modul'] ?></td>
                    <td><?= $p['siri'] ?></td>
                    <td><?= $p['kumpulan_sasaran'] ?></td>
                    <td><?= $p['pemilik'] ?></td>
                    <td><?= $p['anggaran_BilPeserta'] ?></td>
                    <td><?= $p['jumlah_PLelaki'] ?></td>
                    <td><?= $p['jumlah_PPerempuan'] ?></td>
                    <td><?= $p['tempat'] ?></td>
                    <td><?= $p['tarikh_cadangan'] ?></td>
                    <td><?= $p['tempoh'] ?></td>
                    <td><?= $p2->getStatus($p['status_POL']) ?></td>
                    <td><?= $p['peruntukan_diluluskan'] ?></td>
                    <td><?= $p['kos_digunakan'] ?></td>
                    <td><?= $p['anggaran_BPenceramah'] ?></td>
                    <td><?= $p['peruntukan_diluluskan'] - $p['kos_digunakan'] ?> </td>
                    <td><?= $p['catatan_POL'] ?></td>
                </tr>
                <?php endforeach; ?>

                <tr>
                    <td colspan="13" class="text-end">JUMLAH</td>
                    <td><?= $jumlah ?></td>
                    <td colspan="3"></td>
                </tr>
            </tbody>
        </table>
    </div> <?php 
endforeach; ?>

<div class="col-md-6">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th width='70%'></th>
                <th width='15%'>INHOUSE</th>
                <th width='15%'>SWASTA</th>
            </tr>
            <?php
            foreach($arr as $stat) :
            ?>
            <tr>
                <td><?= $stat[0] ?></td>
                <td><?= number_format($stat[1], 0) ?></td>
                <td><?= number_format($stat[2], 0) ?></td>
            </tr>
            <?php endforeach; ?>
        </thead>
    </table>

    <table class="table table-bordered">
        <body>
            <?php
            $tot = $arr[1][1] + $arr[1][2];
            ?>
            <tr>
                <td width='70%'>Peratus</td>
                <td width='15%'><?= $arr[1][1] == 0 ? '-' : $arr[1][1] / $tot * 100 ?></td>
                <td width='15%'><?= $arr[1][2] == 0 ? '-' : $arr[1][2] / $tot * 100 ?></td>
            </tr>
            <tr>
                <td>Jumlah Peruntukan</td>
                <td colspan="2"><?= $total->peruntukan ?></td>
            </tr>
            <tr>
                <td>Perbelanjaan</td>
                <td colspan="2"><?= $total->belanja ?></td>
            </tr>
            </tbody>
    </table>
</div>

<?php
if(isset($print)) : ?>
    <style>
        table tr td, table tr th {
            border: 1px solid #999;
        }
    </style>
<?php endif; ?>