<table class="table table-bordered table-striped" border='1'>
    <thead>
        <tr>
            <th>Bil</th>
            <th>Perkara</th>
            <th>Kepada</th>
            <th>Tarikh Terima</th>
            <th>Sesi TU</th>
            <th>Tarikh TU</th>
            <th>Pengerusi</th>
            <th>Tarikh Penghantaran Kertas Ulasan</th>
            <th>Tarikh Kelulusan Kertas Ulasan</th>
            <th>Tarikh Maklum Kepada Syarikat</th>
            <th>Maklumat Perhubungan</th>
            <th>Status</th>
            <th>Catatan</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        foreach($tu as $t) :
        ?>
        <tr>
            <td><?= $no++ ?></td>
            <td><?= $t['perkara'] ?></td>
            <td><?= $t['kepada'] ?></td>
            <td><?= $t['tarikh_terima'] ?></td>
            <td><?= $t['sesi_tu'] ?></td>
            <td><?= $t['tarikh_tu'] ?></td>
            <td><?= $t['pengerusi_tu'] ?></td>
            <td><?= $t['tarikh_PKUlasan'] ?></td>
            <td><?= $t['tarikh_KKUlasan'] ?></td>
            <td><?= $t['tarikh_MKSyarikat'] ?></td>
            <td><?= $t['maklumat_perhubungan'] ?></td>
            <td><?= \App\Models\TU::getStatus($t['status_tu']) ?></td>
            <td><?= $t['catatan_tu'] ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<div class="row">
    <div class="col-md-4">
        <table class="table table-bordered table-striped" border='1'>
            <?php foreach($stat as $s) : ?>
            <tr>
                <td><?= $s[0] ?></td>
                <td><?= $s[1] ?></td>
            </tr>
            <?php endforeach; ?>

            <tr>
                <td>JUMLAH</td>
                <td><?= $jumlah ?></td>
            </tr>
        </table>
    </div>
    <div class="col-md-8">
        <table class="table table-bordered table-striped" border='1'>
            <tr>
                <td></td>
                <td>MDN</td>
                <td>KSU</td>
                <td>TKSU(P)</td>
                <td>SUB(IT)</td>
            </tr>
            <?php foreach($stat2 as $s) : ?>
            <tr>
                <td><?= $s[1] ?></td>
                <td><?= $s[2] ?></td>
                <td><?= $s[3] ?></td>
                <td><?= $s[4] ?></td>
                <td><?= $s[5] ?></td>
            </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>

<?php
if(isset($print)) : ?>
    <style>
        table tr td, table tr th {
            border: 1px solid #999;
        }
    </style>
<?php endif; ?>