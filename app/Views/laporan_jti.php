<?= $this->extend('master') ?>
<?= $this->section('content') ?>

<form action="/report-jti" method="post">
    Tahun :
    <select name="tahun">
        <option value="">-- Sila Pilih --</option>
        <?php
        $curr_year = date('Y');
        for($i = $curr_year; $i > $curr_year - 5; $i--) {
            $selected = $i == $data['tahun'] ? 'selected' : '';
            echo "<option $selected>$i</option>";
        }
        ?>
    </select>

    Bulan :
    <select name="bulan">
        <option value="">-- Sila Pilih --</option>
        <?php
        foreach($bulan as $kod => $nama) {
            $selected = $kod == $data['bulan'] ? 'selected' : '';
            echo "<option value='$kod' $selected>$nama</option>";
        }
        ?>
    </select>

    Jabatan :
    <select name="jabatan">
        <option value="all">Semua Jabatan</option>
        <?php
        foreach($arr_jab2 as $kod_jab => $keterangan_jab) {
            $selected = $kod_jab == $data['jabatan'] ? 'selected' : '';
            echo "<option value='$kod_jab' $selected>$keterangan_jab</option>";
        }
        ?>
    </select>

    <input type="submit" value="Cari">
</form>

<?php include 'laporan_jti_detail.php' ?>

<a href="/report-jti?print=true&tahun=<?= $data['tahun'] ?>&bulan=<?= $data['bulan'] ?>&jabatan=<?= $data['jabatan'] ?>" class="btn btn-primary">Pdf</a>
<a href="/report-jti?excel=true&tahun=<?= $data['tahun'] ?>&bulan=<?= $data['bulan'] ?>&jabatan=<?= $data['jabatan'] ?>" class="btn btn-primary">Excel</a>

<?= $this->endSection() ?>