
<h4 class="text-center">
    LAPORAN JAWATANKUASA TEKNIKAL ICT(JTI) <br>
    KEMENTERAN DALAM NEGERI
</h4>

<table class="table table-bordered">
    <thead>
        <tr>
            <th>Bil</th>
            <th>Tarikh</th>
            <th>Pengerusi</th>
            <th>No</th>
            <th>Jabatan</th>
            <th>Nama Ringkas Projek</th>
            <th>Kos (RM)</th>
            <th>Tempoh</th>
            <th>Sumber Peruntukan</th>
            <th>Ulasan JTI</th>
            <th>Tempoh Terima Permohonan</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 0;
        $bil = 0;
        foreach($rows as $jti) : 
            if ($bil !== $jti['bil']) {
                $bil = $rs[$i]->bil;
                $rowspan = "rowspan='{$rs[$i]->count}'";
                $i++;
            } else {
                $rowspan = '';
            }
        ?>
        <tr>
            <?php if(! empty($rowspan)) echo "<td $rowspan>{$jti['bil']}</td>"; ?>
            <?php if(! empty($rowspan)) echo "<td $rowspan>{$jti['tarikh']}</td>"; ?>
            <?php if(! empty($rowspan)) echo "<td $rowspan>{$jti['pengerusi']}</td>"; ?>
            <td><?= $jti['no'] ?></td>
            <?php if(! empty($rowspan)) echo "<td $rowspan>{$arr_jab2[$jti['kod_jabatan']]}</td>"; ?>
            <td><?= $jti['tajuk'] ?></td>
            <td><?= $jti['kos'] ?></td>
            <?php if(! empty($rowspan)) echo "<td $rowspan>{$jti['tempoh']}</td>"; ?>
            <?php if(! empty($rowspan)) echo "<td $rowspan>{$jti['sumber_peruntukan']}</td>"; ?>
            <?php if(! empty($rowspan)) echo "<td $rowspan>{$jti['ulasan_jti']}</td>"; ?>
            <td><?= $jti['tarikh_terima_permohonan'] ?></td>
        </tr>
        <?php 
        endforeach; ?>

        <?php
        if (count($rows) == 0) 
            echo "<tr><td colspan='11' align='center'>-- Tiada Rekod Ditemui --</td></tr>";
        ?>
    </tbody>
</table>

<?php
if(isset($print)) : ?>
    <style>
        table, th, td {
            border: 1px solid #999;
            border-collapse: collapse;
        }

        /*tr:nth-child(even) {
            background-color: #eee;
        }*/
    </style>
<?php endif; ?>