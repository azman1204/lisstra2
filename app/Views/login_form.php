<?= $this->extend('master') ?>
<?= $this->section('content') ?>

<div class="row">
    <div class="col-md-6">
        <label>ID Pengguna</label>
        <input type="text" class="form-control">
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <label>Katalaluan</label>
        <input type="password" class="form-control" id='katalaluan'>
    </div>
    <div class="col-md-1">
        <div class="bi bi-eye-slash mt-3 btn btn-outline-primary" 
        style="font-size: 20px; color:blue;" id="mata"></div>
    </div>
</div>

<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script>
    $(function() {
        var show = false; // mata tertutup
        $('#mata').click(function() {
            //alert('you clicked me');
            if (show) {
                // mata terbuka
                $(this).removeClass('bi-eye');
                $(this).addClass('bi-eye-slash');
                $('#katalaluan').attr('type', 'password');
                show = false;
            } else {
                // mata tertutup
                $(this).removeClass('bi-eye-slash');
                $(this).addClass('bi-eye');
                // tukar <input type='password'> kpd <input type='text'>
                $('#katalaluan').attr('type', 'text');
                show = true;
            }
        });
    });
</script>
<?= $this->endSection() ?>