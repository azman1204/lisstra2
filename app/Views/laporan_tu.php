<?= $this->extend('master') ?>
<?= $this->section('content') ?>
<h4>LAPORAN TEKNOLOGI UPDATE</h4>

<form action="/report-tu" method="post">
    Tahun :
    <select name="tahun">
        <option value="">-- Sila Pilih --</option>
        <?php
        $curr_year = date('Y');
        for($i = $curr_year; $i > $curr_year - 5; $i--) {
            $selected = $i == $data['tahun'] ? 'selected' : '';
            echo "<option $selected>$i</option>";
        }
        ?>
    </select>

    Bulan :
    <select name="bulan">
        <option value="">-- Sila Pilih --</option>
        <?php
        foreach($bulan as $kod => $nama) {
            $selected = $kod == $data['bulan'] ? 'selected' : '';
            echo "<option value='$kod' $selected>$nama</option>";
        }
        ?>
    </select>

    Status :
    <select name="status">
        <option value="">-- Semua Status --</option>
        <?php
        foreach($status as $row) {
            $selected = $row['kod_tu'] == $data['status'] ? 'selected' : '';
            echo "<option value='{$row['kod_tu']}' $selected>
                    {$row['keterangan_tu']}
                  </option>";
        }
        ?>
    </select>

    <input type="submit" value="Cari">
</form>

<?php include 'laporan_tu_detail.php' ?>

<a href="/report-tu?print=true&tahun=<?= $data['tahun'] ?>&bulan=<?= $data['bulan'] ?>&status=<?= $data['status'] ?>" class="btn btn-primary">Pdf</a>
<a href="/report-tu?excel=true&tahun=<?= $data['tahun'] ?>&bulan=<?= $data['bulan'] ?>&status=<?= $data['status'] ?>" class="btn btn-primary">Excel</a>

<?= $this->endSection() ?>