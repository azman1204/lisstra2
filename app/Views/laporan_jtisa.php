<?= $this->extend('master') ?>
<?= $this->section('content') ?>

<form action="/report-jtisa" method="post" class="m-4 p-2" style="background-color: #eee;">
    Tahun :
    <select name="tahun">
        <option value="">-- Sila Pilih --</option>
        <?php
        $curr_year = date('Y');
        for($i = $curr_year; $i > $curr_year - 5; $i--) {
            echo "<option>$i</option>";
        }
        ?>
    </select>

    Bulan :
    <select name="bulan">
        <option value="">-- Sila Pilih --</option>
        <?php
        foreach($bulan as $kod => $nama) {
            echo "<option value='$kod'>$nama</option>";
        }
        ?>
    </select>

    Jabatan :
    <select name="jabatan">
        <option value="all">Semua Jabatan</option>
        <?php
        foreach($arr_jab as $jab) {
            echo "<option value='{$jab['kod_jabatan']}'>{$jab['keterangan_jabatan']}</option>";
        }
        ?>
    </select>

    <input type="submit" value="Cari">
</form>

<?php include 'laporan_jtisa_detail.php' ?>

<a href="/report-jtisa?print=true&tahun=<?= $data['tahun'] ?>&bulan=<?= $data['bulan'] ?>&jabatan=<?= $data['jabatan'] ?>" class="btn btn-primary">Pdf</a>
<a href="/report-jtisa?excel=true&tahun=<?= $data['tahun'] ?>&bulan=<?= $data['bulan'] ?>&jabatan=<?= $data['jabatan'] ?>" class="btn btn-primary">Excel</a>

<?= $this->endSection() ?>