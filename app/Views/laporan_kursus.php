<?= $this->extend('master') ?>
<?= $this->section('content') ?>

<form action="/report-kursus" method="post">
    Tahun :
    <select name="tahun">
        <option value="">-- Sila Pilih --</option>
        <?php
        $curr_year = date('Y');
        for($i = $curr_year; $i > $curr_year - 5; $i--) {
            echo "<option>$i</option>";
        }
        ?>
    </select>

    Bulan :
    <select name="bulan">
        <option value="">-- Sila Pilih --</option>
        <?php
        foreach($bulan as $kod => $nama) {
            echo "<option value='$kod'>$nama</option>";
        }
        ?>
    </select>

    Status :
    <select name="status">
        <option value="">-- Sila Pilih --</option>
        <?php 
        foreach($status as $s) {
            echo "<option value='{$s['kod_statusPOL']}'>{$s['keterangan_statusPOL']}</option>";
        }
        ?>
    </select>

    POL :
    <select name="pol">
        <option value="">-- Sila Pilih --</option>
        <option value="01">INHOUSE</option>
        <option value="02">SWASTA</option>
    </select>

    <input type="submit" value="Cari">
</form>

<?php include 'laporan_kursus_detail.php' ?>

<a href="/report-kursus?print=true&tahun=<?= $data['tahun'] ?>&bulan=<?= $data['bulan'] ?>&status=<?= $data['status'] ?>&pol=<?= $data['pol'] ?>" class="btn btn-primary">Pdf</a>
<a href="" class="btn btn-primary">Excel</a>

<?= $this->endSection('content') ?>