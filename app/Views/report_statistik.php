<?= $this->extend('master') ?>
<?= $this->section('content') ?>

<form action="/report-statistik" method="post" class="m-4">
    Tahun :
    <select name="tahun">
        <option value="">-- Sila Pilih --</option>
        <?php
        $curr_year = date('Y');
        for($i = $curr_year; $i > $curr_year - 5; $i--) {
            echo "<option>$i</option>";
        }
        ?>
    </select>

    Jabatan :
    <select name="jabatan">
        <option value="all">Semua Jabatan</option>
        <?php
        foreach($arr_jab as $jab) {
            echo "<option value='{$jab['kod_jabatan']}'>{$jab['keterangan_jabatan']}</option>";
        }
        ?>
    </select>

    <input type="submit" value="Cari">
</form>

<h4 class="text-center">STATISTIK KELULUSAN PROJEK ICT <?= empty($data['tahun']) ? '' : 'PADA' ?> 
<?= $data['tahun'] ?>  SEHINGGA 
<?php 
if (date('Y') == $data['tahun']) 
    echo date('d-m-Y'); 
else if (empty($data['tahun']))
    echo date('d-m-Y');
else 
    echo '31-12-' . $data['tahun'];

echo " BAGI ";

if ($data['jabatan'] == 'all')
    echo 'Semua KDN / Jabatan';
else
    echo $arr_jab2[$data['jabatan']];
?>  
<br>
KEMENTERIAN DALAM NEGERI
</h4>

<table class="table table-bordered table-striped">
    <thead>
        <tr class="text-center">
            <td rowspan="3" style="background-color: #ffe669;">Bil</td>
            <td rowspan="3" style="background-color: #ffe669;">Agensi</td>
            <td rowspan="3" style="background-color: #ffe669;">KC</td>
            <td rowspan="3" style="background-color: #92d050;">Baru</td>
            <td colspan="3" style="background-color: #ffff8b;">JTI</td>
            <td rowspan="3" style="background-color: #ed7d31;" class="text-vertical align-middle">Tidak Diangkat <br> ke JPICT</td>
            <td rowspan="3" style="background-color: #ed7d31;" class="text-vertical align-middle">Kelulusan SU/LP</td>
            <td colspan="5" style="background-color: #bdd7ee;">JPICT</td>
            <td colspan="5" style="background-color: #ed93e0;">JTISA</td>
        </tr>
        <tr>
            <td rowspan="2" style="background-color: #ffff8b;" class="text-vertical align-middle">Diperaku</td>
            <td rowspan="2" style="background-color: #ffff8b;" class="text-vertical align-middle">Tidak Diperaku</td>
            <td rowspan="2" style="background-color: #ffff8b;" class="text-vertical align-middle">Tarik Permohonan</td>
            <td colspan="2" style="background-color: #bdd7ee;">Dalam Tindakan</td>
            <td colspan="2" style="background-color: #bdd7ee;">Lulus</td>
            <td rowspan="2" style="background-color: #bdd7ee;" class="text-vertical align-middle">Kaji Semula</td>
            <td rowspan="2" style="background-color: #ed93e0;" class="text-vertical align-middle">Tarik Permohonan</td>
            <td rowspan="2" style="background-color: #ed93e0;" class="text-vertical align-middle">Dalam Tindakan <br> Agensi</td>
            <td rowspan="2" style="background-color: #ed93e0;" class="text-vertical align-middle">Pra JTISA</td>
            <td rowspan="2" style="background-color: #ed93e0;" class="text-vertical align-middle">Kaji Semula</td>
            <td rowspan="2" colspan='10' style="background-color: #ed93e0;" class="text-vertical align-middle">Lulus</td>
        </tr>
        <tr class="text-center">
            <td style="background-color: #bdd7ee;" class="text-vertical align-middle">Agensi</td>
            <td style="background-color: #bdd7ee;" class="text-vertical align-middle">Urusetia</td>
            <td style="background-color: #bdd7ee;" class="text-vertical align-middle">MC</td>
            <td style="background-color: #bdd7ee;" class="text-vertical align-middle">MSY</td>
        </tr>
    </thead>
    <tbody>
        <?php 
        foreach ($arr1 as $arr) : ?>
        <tr class="text-center">
            <td><?= $arr[0] ?></td>
            <td class="text-start"><?= $arr[19] ?></td>
            <td><?= $arr[2] ?></td>
            <td><?= $arr[3] ?></td>
            <td><?= $arr[4] ?></td>
            <td><?= $arr[5] ?></td>
            <td><?= $arr[6] ?></td>
            <td><?= $arr[7] ?></td>
            <td><?= $arr[8] ?></td>
            <td><?= $arr[9] ?></td>
            <td><?= $arr[10] ?></td>
            <td><?= $arr[11] ?></td>
            <td><?= $arr[12] ?></td>
            <td><?= $arr[13] ?></td>
            <td><?= $arr[14] ?></td>
            <td><?= $arr[15] ?></td>
            <td><?= $arr[16] ?></td>
            <td><?= $arr[17] ?></td>
            <td><?= $arr[18] ?></td>
        </tr>
        <?php endforeach; ?>
        <tr class='jumlah text-center'>
            <td colspan='2'>JUMLAH</td>
            <td><?= $arr_jum[0] ?></td>
            <td><?= $arr_jum[1] ?></td>
            <td><?= $arr_jum[2] ?></td>
            <td><?= $arr_jum[3] ?></td>
            <td><?= $arr_jum[4] ?></td>
            <td><?= $arr_jum[5] ?></td>
            <td><?= $arr_jum[6] ?></td>
            <td><?= $arr_jum[7] ?></td>
            <td><?= $arr_jum[8] ?></td>
            <td><?= $arr_jum[9] ?></td>
            <td><?= $arr_jum[10] ?></td>
            <td><?= $arr_jum[11] ?></td>
            <td><?= $arr_jum[12] ?></td>
            <td><?= $arr_jum[13] ?></td>
            <td><?= $arr_jum[14] ?></td>
            <td><?= $arr_jum[15] ?></td>
            <td><?= $arr_jum[16] ?></td>
        </tr>
    </tbody>
</table>

<style>
    tr.jumlah td{
        background-color: black;
        color: white;
    }
.text-vertical{
    writing-mode: vertical-rl;
    text-orientation: revert;
    transform: rotate(180deg);
}

 thead {
    font-weight: bold;
    font-size: 0.8em;
 }
</style>

<?= $this->endSection() ?>