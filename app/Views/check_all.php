<?= $this->extend('master') ?>
<?= $this->section('content') ?>

<table class="table">
    <tr>
        <th><input type="checkbox" id="my-checkbox"></th>
        <th>NO</th>
    </tr>
    <?php foreach([1,2,3,4,5] as $no) : ?>
    <tr>
        <td><input type="checkbox"></td>
        <td><?= $no ?></td>
    </tr>
    <?php endforeach; ?>
</table>

<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script>
    $(function() {
        let checked = false;
        $('#my-checkbox').click(function() {
            if (checked)
                $('input:checkbox').prop('checked', false);
            else
                $('input:checkbox').prop('checked', true);

            checked = ! checked;
        });
    });
</script>
<?= $this->endSection() ?>