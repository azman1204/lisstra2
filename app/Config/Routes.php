<?php
use CodeIgniter\Router\RouteCollection;

$routes->get('/', 'Home::index');
$routes->get('/hello-world', 'HelloController::index');
$routes->get('/gen-pdf', 'HelloController::genPdf');
$routes->match(['get', 'post'], '/report-statistik', 'Report2Controller::index');
$routes->match(['get', 'post'], '/report-jti', 'Report2Controller::laporanJTI');
$routes->match(['get', 'post'], '/report-jtisa', 'Report2Controller::laporanJTISA');
$routes->match(['get', 'post'], '/report-kursus', 'Report2Controller::laporanKursus');
$routes->match(['get', 'post'], '/report-tu', 'Report2Controller::laporanTu');
$routes->get('/report-tu-stat', 'Report2Controller::stat');
$routes->get('/login', 'LoginController::index');
$routes->get('/check-all', 'LoginController::checkAll');
